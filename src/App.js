// Cite: Kano Marvel, David Sreesangkom 
import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';



class App extends Component {
  state = {
    todos: todosList,
    value:""
  };
  
  handleSubmit = (event) => {
    if (event.key === "Enter") {
      
      this.handleAddTodo()
    }
  }
  handleAddTodo = () => {
    const newTodo ={
      "userId" :1,
      "id": uuidv4(),
      "title" : this.state.value,
      "completed": false
    }
  const newTodos = [...this.state.todos]
      this.setState({
        todos:newTodos,
        value:""
      })
  
  }
  handleDelete = (todoId) => {
    const newTodos = this.state.todos.filter(
      todoItem => todoItem.id !== todoId
    )
    this.setState({ todos: newTodos })
  }
  handleCheck =(checkId)=>{
    const newTodos = this.state.todos.map(
      todoItem => {
        if (todoItem.id === checkId){
          todoItem.completed = !todoItem.completed
        }
        return todoItem
      }
    )
    this.setState({ todos: newTodos })
  }
handleDeleteDone = () =>{
  const newTodos = this.state.todos.filter(
    todoItem => todoItem.completed === false
  )
  this.setState({ todos: newTodos })
}

  newTodo = 
    {
      "userId": 1,
      "id": 1,
      "title": "delectus aut autem",
      "completed": false
    }

  handleNewText =(event)=>{
    let freshId 
    let freshTodoItem
    if (event.keyCode===13){
      freshId=this.state.todos.length+1
      let content = document.getElementById("addNewTextToDo").value
      freshTodoItem = {completed:false, id:freshId, title:content, userId:1}
      this.setState({todos: this.state.todos.concat(freshTodoItem)
      })
    }
    
    this.setState({value: event.target.value})
  }
 

  
  render() {
    document.addEventListener("keydown",this.handleNewText)
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          type= "text"
          onChange= {this.handleNewText}
          onKeyDown ={this.handleSubmit}
          value= {this.state.value}
          id = "addNewTextToDo" 
          className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus />
        </header>
        <TodoList 
        todos={this.state.todos} 
        handleCheck = {this.handleCheck}
        handleDelete={this.handleDelete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button  
          onClick = {this.handleDeleteDone} 
          className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          onChange ={()=>this.props.handleCheck(this.props.id)}
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed} />
          <label>{this.props.title}</label>
          <button 
            className="destroy"
            onClick ={() => this.props.handleDelete(this.props.id)} 
            />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            handleCheck = {this.props.handleCheck}
            handleDelete= {this.props.handleDelete} 
            title={todo.title} 
            completed={todo.completed} 
            id= {todo.id}
            key={todo.id} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;